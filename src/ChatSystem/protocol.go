package ChatSystem


type Message struct {
	From string
	To string
	Content string
}

type ClientInfo struct {
	Id string
	Users map[string][]Message
	Msg string
}

type SystemInfo struct {
	Users []string
}

type Dummy struct {
	dmy int
}


