package main

import (
	"sync"
	"os"
	"strings"
	"fmt"
	"net/rpc"
	"net/http"
	"ChatSystem"
	"strconv"
)

// dummy type
//type ChatServer int

type ChatServer struct {
	mutex *sync.Mutex
	users map[string][]ChatSystem.Message
}

func NewChatServer() *ChatServer {
	return &ChatServer{
		mutex: new(sync.Mutex),
		users: make(map[string][]ChatSystem.Message),
	}
}

// send user login message to all user online
func (server *ChatServer) Register(client *ChatSystem.ClientInfo, reply *ChatSystem.ClientInfo) error {
	fmt.Printf("user %s want to join system\n", client.Id)
	server.mutex.Lock()
	defer server.mutex.Unlock()

	// access the code
	// what if not exist?

	reply.Msg = "Hello new user!"
	// could be bug,
	server.users[client.Id] = server.users[client.Id]
	for user, messages := range server.users {
		if user != client.Id {
			// update
			server.users[user] = append(messages, ChatSystem.Message{
				From: client.Id,
				To: user,
				Content: "User " + client.Id + " just loggedin",
			})
		}
	}

	reply.Users = server.users
	return nil
}

func (server*ChatServer) CheckMessage(client *ChatSystem.ClientInfo, replay *[]string) error {
	server.mutex.Lock()
	defer server.mutex.Unlock()

	//*replay = server.users[client.Id]
	for _, content := range server.users[client.Id] {
		*replay = append(*replay, content.Content)
	}

	server.users[client.Id] = []ChatSystem.Message{} // empty it

	return nil
}

//func Say() error  {
//
//}

func (server *ChatServer) Tell(client *ChatSystem.Message, reply *string) error {
	server.mutex.Lock()
	defer server.mutex.Unlock()

	content := client.From + " tells you " + client.Content
	server.users[client.To] = append(server.users[client.To],
		ChatSystem.Message{
			From: client.From,
			To: client.To,
			Content:content})

	return nil
}


//func List()  error {
//
//}
//

//
//func Logout()  {
//
//}
//
//func ShutdownSystem()  {
//
//}

func main() {
	var port = 3410

	// 2 means 1 argument, the first one is default argument
	if len(os.Args) == 2 {
		port, _ = strconv.Atoi(strings.TrimLeft(os.Args[1], "-port="))
		fmt.Printf("register to port %d\n", port)
	}

	chatServer := NewChatServer()
	rpc.Register(chatServer)
	rpc.HandleHTTP()

	err := http.ListenAndServe(":" + strconv.Itoa(port), nil)

	if err != nil {
		fmt.Printf("Error in setup chatserver %s\n", err)
	}

}