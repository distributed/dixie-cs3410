package main

import (
	"bufio"
	"os"
	"fmt"
	"net"
	"net/rpc"
	"ChatSystem"
	"time"
	"strings"
)

func connectToServer(clientId string, ipPort string) (*rpc.Client, error)  {
	fmt.Printf("Hi %s, connecting to %s...\n", clientId, ipPort)
	client, err := rpc.DialHTTP("tcp", ipPort)
	return client, err
}

func login(client *rpc.Client, clientInfo *ChatSystem.ClientInfo) {
	reply := ChatSystem.ClientInfo{}
	client.Call("ChatServer.Register", clientInfo, &reply)
	fmt.Printf("got message returned by server which says that {%s}\n", reply.Msg)
	fmt.Printf("List of users currently online:\n")

	for k, _ := range reply.Users {
		fmt.Printf(k + "\n")
		//fmt.Println()
	}
}

func checkMessage(client *rpc.Client, clientInfo *ChatSystem.ClientInfo)  {
	//fmt.Println("try to fetch message from server...")
	reply := []string {}
	client.Call("ChatServer.CheckMessage", clientInfo, &reply)

	for _, content := range reply {
		fmt.Printf(content + "\n")
	}
}

func checkMessageRoutine(client *rpc.Client, clientInfo *ChatSystem.ClientInfo)  {
	for range time.Tick(time.Second * 1) {
		checkMessage(client, clientInfo)
	}

}

func say(client *rpc.Client, clientId string, content *string)  {

}

func tell(client *rpc.Client, message *ChatSystem.Message) {
	// very hard to reason when using pointer, and when using struct itself
	reply := new(string)
	client.Call("ChatServer.Tell", message, reply)
}

//func list(client *rpc.Client, clientId string)[]string {
//
//}
//
//func quit(client *rpc.Client, clientId string) {
//
//}
//
//func shutdownServer()  {
//
//}

func help() {
	fmt.Printf("Usage:")
	fmt.Printf("Login with: ./client $NAME, or ./client $NAME :$PORT or ./client $NAME $IP:$PORT")
	fmt.Printf("After login you could do:")
	fmt.Printf("tell $NAME $MESSAGE")
	fmt.Printf("say $MESSAGE")
	fmt.Printf("list")
	fmt.Printf("quit")
	fmt.Printf("help")
	fmt.Printf("shutdown")
}


func main() {

	var serverAddress string
	var mport string = "3410"
	var clientId string

	if len(os.Args) == 2 {
		fmt.Println("argument is 2")
		clientId = os.Args[1]
	} else if len(os.Args) == 3 {
		clientId = os.Args[1]
		host, port, err := net.SplitHostPort(os.Args[2])

		mport = port
		if err != nil {
			fmt.Printf("failed to parse host port\n")
			os.Exit(1)
		}

		if(host == "") {
			serverAddress = "localhost"
		}

	} else {
		fmt.Printf("argument size not 1 or 2\n")
		os.Exit(1)
	}

	// connect to server
	client, err := connectToServer(clientId, serverAddress+":"+mport)

	if err != nil {
		fmt.Printf("Failed to connect to server\n")
	}

	clientInfo := ChatSystem.ClientInfo{
		Id:clientId}

	login(client, &clientInfo)

	go checkMessageRoutine(client, &clientInfo)

	scanner := bufio.NewScanner(os.Stdin)

	// listen command from user
	for true {
		//fmt.Println("in for true")
		//input := []string {}

		reader := bufio.NewReader(os.Stdin)
		text, _ := reader.ReadString('\n')
		input := strings.Split(text, " ")

		//for scanner.Scan() {
		//	input = append(input, scanner.Text())
		//	//fmt.Printf(scanner.Text()) // Printf will add back the final '\n'
		//}

		if input[0] == "tell" {
			// tell
			toUser := input[1]
			content := strings.Join(input[2:]," ")
			//fmt.Println("you just said: " + content)

			message := ChatSystem.Message {
				From:clientId,
				To:toUser,
				Content:content,
			}
			tell(client, &message)
		}

		if err := scanner.Err(); err != nil {
			//fmt.Printf(os.Stderr, "reading standard input:\n")
		}
	}

}