package main

import (
	"testing"
	"fmt"
	"reflect"
)

func TestTypeOf(t *testing.T) {
	fmt.Println("hello world")
}

func main() {

	type T struct {
		A int
		B string
	}

	t := T{23, "skidoo"}

	s := reflect.ValueOf(&t).Elem()

	typeOfT := s.Type()

	for i := 0; i < s.NumField(); i++ {
		f := s.Field(i)
		fmt.Printf("%d: %s %s = %v\n", i, typeOfT.Field(i).Name, f.Type(), f.Interface())
	}

}
