package main

import (
	"net/rpc"
	"fmt"
	"log"
	"os"
)

/*
The client needs to set up an HTTP connection to the RPC server. It needs to prepare a
structure with the values to be sent, and the address of a variable to store the results in.
Then it can make a Call with arguments:

	The name of the remote function to execute
	The values to be sent
	The address of a variable to store the result in

A client that calls both functions of the arithmetic server is
 */

type Args struct {
	A, B int
}

type Quotient struct {
	Quo, Rem int
}

/**
只需要传入一个参数 localhost
 */
func main() {
	if len(os.Args) != 2 {
		fmt.Println("Usage: ", os.Args[0], "server")
		os.Exit(1)
	}

	serverAddress := os.Args[1]

	client, err := rpc.DialHTTP("tcp", serverAddress+":45678")

	if err != nil {
		log.Fatal("dialing:", err)
	}

	// Synchronous call
	args := Args {17, 8}
	var reply int
	err = client.Call("Arith.Multiply", args, &reply)

	if err != nil {
		log.Fatal("arith error:", err)
	}

	fmt.Printf("Arith: %d*%d=%d\n", args.A, args.B, reply)

	var quot Quotient
	err = client.Call("Arith.Divide", args, &quot)

	if err != nil {
		log.Fatal("arith error:", err)
	}

	fmt.Printf("Arith: %d/%d=%d remainder %d\n", args.A, args.B, quot.Quo, quot.Rem)
}
