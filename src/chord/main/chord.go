package main

import (
	"sync"
	"math/big"
	"chord"
	"log"
)

type Node struct {
	mu          sync.Mutex
	Address     string
	predecessor string
	successor   [3]string
	fingerTbl   []string
	kvs map[string]string
}

// rpc can have but one input in request
type KeyValue struct {
	key string
	value string
	replace bool
}

// This computes the address of a position across the ring that should be pointed to by the given finger table entry
//(using 1-based numbering).
// !! if that table not exist, then go to other node
func (elt *Node) jump(fingerentry int) *big.Int {
	n := chord.HashString(elt.Address)
	two := big.NewInt(2)
	fingerentryminus1 := big.NewInt(int64(fingerentry) - 1)
	// z = x ^ y
	jump := new(big.Int).Exp(two, fingerentryminus1, nil)
	sum := new(big.Int).Add(n, jump)
	return new(big.Int).Mod(sum, chord.HashMod)
}

/**
 */
func (elt *Node) closestPrecedingFinger(id *big.Int) string {
	for i := len(elt.fingerTbl) - 1; i >= 0; i -- {
		rmt := elt.fingerTbl[i]
		if chord.Between(chord.HashString(elt.Address), chord.HashString(rmt), id, true) {
			return rmt
		}
	}
	panic("should not happen")
}

//@todo set maximum rpc number
func (elt *Node) FindPredecessor(id *big.Int, addr *string) error {

	for ! chord.Between(chord.HashString(elt.Address),
		id,
		chord.HashString(elt.successor[0]), true) {

		// the nearest node this node knows
		potentialPredecessor := elt.closestPrecedingFinger(id)
		chord.Call(potentialPredecessor, "Node.FindPredecessor", id, addr)
	}
	return nil
}

func (elt *Node) FindSuccessor(id *big.Int, addr *string) error {
	var predecessor string
	elt.FindPredecessor(id, &predecessor)
	chord.Call(predecessor, "Node.Successor", nil, addr)
	return nil
}

func (elt *Node) Successor(nothing interface{}, addr *string) error {

	addr = &elt.successor[0]
	return nil
}

func Join(seed string, addr string) *Node {
	id := chord.HashString(addr)

	var predecessor string
	chord.Call(seed, "Node.FindPredecessor", id, &predecessor)

	var successor string
	chord.Call(seed, "Node.FindSuccessor", id, &successor)

	// when to add the remaining successors
	newNode := Node {
		mu: sync.Mutex{},
		Address: addr,
		successor:[3]string{successor,"",""},
		predecessor:predecessor,
		fingerTbl:[]string{},
		kvs:map[string]string{},
	}

	// from 0 or from 1?
	for i:= 1; i < 161; i ++ {
		neighboorId := newNode.jump(i)
		var neighboorAddr string
		chord.Call(seed, "Node.FindSuccessor", neighboorId, &neighboorAddr)
		newNode.fingerTbl[i] = neighboorAddr
	}

	return &newNode
}

// use interface

// elt is the destination node of key
func (elt *Node) Get(key string, value *string) error {
	//var destNodeAddr string
	//id := chord.HashString(key)
	//elt.FindSuccessor(id, &destNodeAddr)

	if val, ok := elt.kvs[key]; ok {
		*value = val
		return nil
	}

	log.Fatal("key[%s] is not on server[%s]", key, elt.Address)
	return nil
}

func (elt *Node) Put(kv KeyValue, status *bool) error {
	if _, ok := elt.kvs[kv.key]; ok {
		if(!kv.replace) {
			*status = false
		}
 	}

	elt.kvs[kv.key] = kv.value
	*status = true
	return nil
}

func (elt *Node) Delete(key string, status *bool) error {
	if _, ok := elt.kvs[key]; ok {
		*status = true
		delete(elt.kvs, key)
	}
	return nil
}

//func main() {
//	two := big.NewInt(2)
//	three := big.NewInt(3)
//	jump := new(big.Int).Exp(two, three, nil)
//	log.Printf("value is %d", jump)
//}
