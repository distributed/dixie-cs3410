package main

import (
	"testing"
	"fmt"
	"chord"
	"math/big"
)

func TestUtilFunctions(t *testing.T) {
	ipaddr := chord.GetLocalAddress()
	fmt.Printf("ip address is %s\n", ipaddr)

	// test jump function

	node := Node {Address:"0"}
	fg1 := node.jump(1).String() // 0+1 -> 1
	fg2 := node.jump(2).String() // 0+2 -> 2
	fg3 := node.jump(3).String() // 0+4 -> 4

	fmt.Printf("first finger table is [%s], second finger table is [%s], third is [%s]\n",
		fg1, fg2, fg3)
}

// chord paper
// ring size of 8, has nodes 0, 1, 3
// check the finger table of 0, 1, and 3
func TestClosestFingerTable(t *testing.T) {
	// node 0's finger table, 1, 3, 0, that's correct
	node0 := Node{Address:"0", fingerTbl:[]string {"1", "3", "0"}}

	p0 := node0.closestPrecedingFinger(new(big.Int).SetInt64(0))
	p1 := node0.closestPrecedingFinger(new(big.Int).SetInt64(1))
	p2 := node0.closestPrecedingFinger(new(big.Int).SetInt64(2))
	p3 := node0.closestPrecedingFinger(new(big.Int).SetInt64(3))
	p4 := node0.closestPrecedingFinger(new(big.Int).SetInt64(4))
	p5 := node0.closestPrecedingFinger(new(big.Int).SetInt64(5))
	p6 := node0.closestPrecedingFinger(new(big.Int).SetInt64(6))
	p7 := node0.closestPrecedingFinger(new(big.Int).SetInt64(7))

	fmt.Printf("%s[0] %s[1] %s[1] %s[3]" +
		" %s[3] %s[3] %s[3] %s[3]\n", p0, p1, p2, p3, p4, p5, p6, p7)

	if p0 != "0" || p1 != "1" || p2 != "1" || p3 != "3" {
		fmt.Printf("Error happend\n")
	}

	if p4 != "3" || p5 != "3" || p6 != "3" || p7 != "3" {
		fmt.Printf("Error happend\n")
	}
}

func TestNodeJoin(t *testing.T) {
	fmt.Printf("node join @todo\n")
}