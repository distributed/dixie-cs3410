package main

import (
	"chord"
	"fmt"
)

func main()  {

	bigInt1 := chord.HashString("1")
	fmt.Printf("big int for 1 is %s\n", bigInt1.String())

	bigInt2 := chord.HashString("2")
	fmt.Printf("big int for 2 is %s\n", bigInt2.String())

	bigInt3 := chord.HashString("3")
	fmt.Printf("big int for 3 is %s\n", bigInt3.String())
}
