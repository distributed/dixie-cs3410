package chord

import (
	"math/big"
	"crypto/sha1"
	"net"
	"fmt"
	"net/rpc"
)

const DebugMode bool = true

func Call(address string, method string, request interface{}, reply interface{}) error {
	client, err := rpc.DialHTTP("tcp", address)
	if err != nil {
		//log.Printf("\tCommand(%s) Error connecting to server at %s: %v", method, address, err)
		return fmt.Errorf("\tCommand(%s) Error connecting to server at %s: %v", method, address, err)
	}
	defer client.Close()
	return client.Call(method, request, reply)
}

func HashString(elt string) *big.Int {

	if DebugMode {
		res, _ := new(big.Int).SetString(elt, 10)
		return res
	} else {
		hasher := sha1.New()
		hasher.Write([]byte(elt))
		return new(big.Int).SetBytes(hasher.Sum(nil))
	}
}

func Between(start, elt, end *big.Int, inclusive bool) bool {
	if end.Cmp(start) > 0 {
		return (start.Cmp(elt) < 0 && elt.Cmp(end) < 0) || (inclusive && elt.Cmp(end) == 0)
	} else {
		return start.Cmp(elt) < 0 || elt.Cmp(end) < 0 || (inclusive && elt.Cmp(end) == 0)
	}

	panic("impossible")
}

const KeySize = sha1.Size * 8
var HashMod = new(big.Int).Exp(big.NewInt(2), big.NewInt(KeySize), nil)


func GetLocalAddress() string {
	var localaddress string

	ifaces, err := net.Interfaces()
	if err != nil {
		panic("init: failed to find network interfaces")
	}

	// find the first non-loopback interface with an IP address
	for _, elt := range ifaces {
		if elt.Flags&net.FlagLoopback == 0 && elt.Flags&net.FlagUp != 0 {
			addrs, err := elt.Addrs()
			if err != nil {
				panic("init: failed to get addresses for network interface")
			}

			for _, addr := range addrs {
				if ipnet, ok := addr.(*net.IPNet); ok {
					if ip4 := ipnet.IP.To4(); len(ip4) == net.IPv4len {
						localaddress = ip4.String()
						break
					}
				}
			}
		}
	}
	if localaddress == "" {
		panic("init: failed to find non-loopback interface with valid address on this node")
	}

	return localaddress
}


