package main

import (
	"errors"
	"net/rpc"
	"net"
	"log"
	"net/http"
	"fmt"
)

type Args struct {
	A, B int
}

type Quotient struct {
	Quo, Rem int
}

type Arith int

func (t *Arith) Multiply(args *Args, reply *int) error {
	*reply = args.A * args.B
	return nil
}

func (t *Arith) Divide(args *Args, quo *Quotient) error {
	if args.B == 0 {
		return errors.New("divide by zero")
	}
	quo.Quo = args.A / args.B
	quo.Rem = args.A % args.B
	return nil
}

func main()  {
	arith := new(Arith)

	rpc.Register(arith)
	rpc.HandleHTTP()

	l, e := net.Listen("tcp", ":23409")

	if e != nil {
		log.Fatal("listen error:", e)
	}

	// go http.Serve(l, nil) will exit execution immediately
	go http.Serve(l, nil)

	serverAddress := "localhost"

	client, err := rpc.DialHTTP("tcp", serverAddress + ":23409")

	if err != nil {
		log.Fatal("dialing:", err)
	}

	// Synchronous call
	args := &Args{7,8}
	var reply int
	err = client.Call("Arith.Multiply", args, &reply)
	if err != nil {
		log.Fatal("arith error:", err)
	}
	fmt.Printf("Arith: %d*%d=%d", args.A, args.B, reply)

	// Asynchronous call
	quotient := new(Quotient)
	divCall := client.Go("Arith.Divide", args, quotient, nil)
	replyCall := <- divCall.Done	// will be equal to divCall


}
