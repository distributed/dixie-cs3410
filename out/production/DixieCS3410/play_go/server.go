package main

import (
	"protocol"
	"net/rpc"
	"net"
	"log"
	"net/http"
)



func main()  {
	arith := new(Arith)

	rpc.Register(arith)
	rpc.HandleHTTP()

	l, e := net.Listen("tcp", ":23409")

	if e != nil {
		log.Fatal("listen error:", e)
	}

	// go http.Serve(l, nil) will exit execution immediately
	http.Serve(l, nil)

}