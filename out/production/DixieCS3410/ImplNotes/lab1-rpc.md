### Before started

Everybody says that it's very easy to use go to implement a RPC server. Let's find out 
if it's true!

### RPC in golang

[一个可以直接 run 的讲解](https://jan.newmarch.name/go/rpc/chapter-rpc.html)

有一点不太明白, client.go 和 server.go 写在同一个目录下, 却不能共享类型声明, 只能在 client.go 和 server.go 上
各自重复一遍类型声明

### 处理用户输入

用户输入的东西是很多 string 需要分别予以处理, 课程网站给出了 Scanner 的例子, 用来处理用户输入

```go
// An artificial input source.
    const input = "1234 5678 1234567901234567890"
    scanner := bufio.NewScanner(strings.NewReader(input))
    
    // Create a custom split function by wrapping the existing ScanWords function.
    split := func(data []byte, atEOF bool) (advance int, token []byte, err error) {
            advance, token, err = bufio.ScanWords(data, atEOF)
            if err == nil && token != nil {
                    _, err = strconv.ParseInt(string(token), 10, 32)
            }
            return
    }
    // Set the split function for the scanning operation.
    scanner.Split(split)
    
    // Validate the input
    for scanner.Scan() {
            fmt.Printf("%s\n", scanner.Text())
    }

    if err := scanner.Err(); err != nil {
            fmt.Printf("Invalid input: %s", err)
    }

// output

1234
5678
Invalid input: strconv.ParseInt: parsing "1234567901234567890": value out of range
```

这里的  split 函数没看明白是怎么 work 的

处理命令行的输入

```go
func main() {
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		fmt.Println(scanner.Text()) // Println will add back the final '\n'
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}
```

### golang 面向对象编程

```go
type ChatServer struct {
	mutex sync.Mutex
	users []string
	pendingMsg map[string][]ChatSystem.Message
}
```


struct 中的成员变量, 如何是大写字母开头, 就是 public 的, 小写就是 private 的

构造函数

```go
You just need a constructor. A common used pattern is

func NewSyncMap() *SyncMap {
    return &SyncMap{hm: make(map[string]string)}
}
In case of more fields inside your struct, starting a goroutine as backend, or registering a finalizer everything could be done in this constructor.

func NewSyncMap() *SyncMap {
    sm := SyncMap{
        hm: make(map[string]string),
        foo: "Bar",
    }
    runtime.SetFinalizer(sm, (*SyncMap).stop)
    go sm.backend()
    return &sm
}

// example2

type SyncMap struct {
        lock *sync.RWMutex
        hm map[string]string
}

func NewSyncMap() *SyncMap {
        return &SyncMap{lock: new(sync.RWMutex), hm: make(map[string]string)}
}

func (m *SyncMap) Put (k, v string) {
        m.lock.Lock()
        defer m.lock.Unlock()
        m.hm[k] = v
}

func main() {
    sm := NewSyncMap() // 初始化就不需要再用 new() 了, 那样没用
    sm.Put("Test", "Test") 
}
```

### 使用锁

```go
server.mutex.Lock()
defer server.mutex.Unlock()
```

more about mutex

```go

// SafeCounter is safe to use concurrently.
type SafeCounter struct {
	v   map[string]int
	mux sync.Mutex
}

// Inc increments the counter for the given key.
func (c *SafeCounter) Inc(key string) {
	c.mux.Lock()
	// Lock so only one goroutine at a time can access the map c.v.
	c.v[key]++
	c.mux.Unlock()
}

// Value returns the current value of the counter for the given key.
func (c *SafeCounter) Value(key string) int {
	c.mux.Lock()
	// Lock so only one goroutine at a time can access the map c.v.
	defer c.mux.Unlock()
	return c.v[key]
}

func main() {
	c := SafeCounter{v: make(map[string]int)}
	for i := 0; i < 1000; i++ {
		go c.Inc("somekey")
	}

	time.Sleep(time.Second)
	fmt.Println(c.Value("somekey"))
}
```

### 类型转换 

string -> int strconv.atoi

int -> string strcov.itoa

### server to all available clients

不知道如何实现, 只看到过 client -> server -> client 的过程, 没看到 server 主动联系 client 的过程
 
**学到的知识点**

1: server rpc 的点

```go
func (room *ChatRoom) Register(user *string, empty *Nothing) error {
```

可见参数不一定是 struct, string 就可以, 此外还有一个类型是 Nothing
发现 Nothing 是自己定义的

2: server 还是不能向 client 发送消息, 是靠 client 的 checkMessage 实现的

### go routine

```go
func f(from string) {
    for i := 0; i < 3; i++ {
        fmt.Println(from, ":", i)
    }
}

go f("goroutine") // 直接调用

go func(msg string) {fmt.Println(msg)}("going") // 创建函数并直接调用
```

参数的确定发生在

chan 是什么? 定时任务怎么实现?

```go
func main() {
	go heartBeat()
	time.Sleep(time.Second * 5)
}

func heartBeat(){
    for range time.Tick(time.Second *1){
        fmt.Println("Foo")
    }
}
```

### chan

channels are typed conduit through which you can send and receive values with the channel operator

```go
ch <- v    // Send v to channel ch.
v := <-ch  // Receive from ch, and
           // assign value to v.
```

like slice and map, channel must be created before use

```go
ch := make(chan int)
```

By default, sends and receives block until the other side is ready. This 
allows goroutines to synchronize without explicit locks or condition variables.

```go
func sum(s []int, c chan int) {
	sum := 0
	for _, v := range s {
		sum += v
	}
	c <- sum // send sum to c
}

func main() {
	s := []int{7, 2, 8, -9, 4, 0}

	c := make(chan int)
	go sum(s[:len(s)/2], c)
	go sum(s[len(s)/2:], c)
	x, y := <-c, <-c // receive from c

	fmt.Println(x, y, x+y)
}
```

从这个例子看, channel kind of like semaphore and next example looks more like a semaphore

```go
func main() {
	ch := make(chan int, 2)
	ch <- 1
	ch <- 2
	fmt.Println(<-ch)
	fmt.Println(<-ch)
}
```

sends to the buffer channel block only when the buffer is full. Receives block when block is 
empty.

This example is a litter bit complex, the range of close of channel

```go
func fibonacci(n int, c chan int) {
	x, y := 0, 1
	for i := 0; i < n; i++ {
		c <- x
		x, y = y, x+y
	}
	close(c)
}

func main() {
	c := make(chan int, 10)
	go fibonacci(cap(c), c)
	for i := range c {
		fmt.Println(i)
	}
}
```

The first thing worth notice is that golang could assign two value simutaneously.

**range** c could receive value from channel repeatly until the channel is closed.

Note that, channel aren't like files. You don't usually need to close them. Closing is 
only necessary when the receiver need to told there are no more value coming, such as 
to terminate a range loop.

**select**

the select statement let go routine wait on multiple communication operations.

A select blocks until one of its cases can run, then it execute the case, it chooses
one at random if multiple is ready.

```go
func fibonacci(c, quit chan int) {
	x, y := 0, 1
	for {
		select {
		case c <- x:
			x, y = y, x+y
		case <-quit:
			fmt.Println("quit")
			return
		}
	}
}

func main() {
	c := make(chan int)
	quit := make(chan int)
	go func() {
		for i := 0; i < 10; i++ {
			fmt.Println(<-c)
		}
		quit <- 0
	}()
	fibonacci(c, quit)
}
```

some times, a default case could be useful, if no other case is ready. So, default usually
work with sleep

```go
func main() {
	tick := time.Tick(100 * time.Millisecond)
	boom := time.After(500 * time.Millisecond)
	for {
		select {
		case <-tick:
			fmt.Println("tick.")
		case <-boom:
			fmt.Println("BOOM!")
			return
		default:
			fmt.Println("    .")
			time.Sleep(50 * time.Millisecond)
		}
	}
}
```

